"use strict";
function _classCallCheck(e, n) {
    if (!(e instanceof n))
        throw new TypeError("Cannot call a class as a function")
}
var _createClass = function() {
    function e(e, n) {
        for (var t = 0; t < n.length; t++) {
            var r = n[t];
            r.enumerable = r.enumerable || !1,
            r.configurable = !0,
            "value"in r && (r.writable = !0),
            Object.defineProperty(e, r.key, r)
        }
    }
    return function(n, t, r) {
        return t && e(n.prototype, t),
        r && e(n, r),
        n
    }
}()
  , Rail = function() {
    function e(n) {
        _classCallCheck(this, e),
        this.ele = document.querySelectorAll(n)
    }
    return _createClass(e, null, [{
        key: "getInstance",
        value: function(n) {
            return e.instances || (e.instances = {}),
            e.instances[n] || (e.instances[n] = new e(n)),
            e.instances[n]
        }
    }, {
        key: "filter",
        value: function(e, n) {
            var t = []
              , r = !0
              , a = !1
              , l = void 0;
            try {
                for (var i, s = e[Symbol.iterator](); !(r = (i = s.next()).done); r = !0) {
                    var o = i.value;
                    n(o) && t.push(o)
                }
            } catch (u) {
                a = !0,
                l = u
            } finally {
                try {
                    !r && s["return"] && s["return"]()
                } finally {
                    if (a)
                        throw l
                }
            }
            return t
        }
    }]),
    _createClass(e, [{
        key: "addClass",
        value: function(n) {
            var t = !0
              , r = !1
              , a = void 0;
            try {
                for (var l, i = this.ele[Symbol.iterator](); !(t = (l = i.next()).done); t = !0) {
                    var s = l.value
                      , o = e.filter(s.className.split(" "), function(e) {
                        return !!e
                    });
                    o.push(n),
                    s.className = o.join(" ")
                }
            } catch (u) {
                r = !0,
                a = u
            } finally {
                try {
                    !t && i["return"] && i["return"]()
                } finally {
                    if (r)
                        throw a
                }
            }
            return this
        }
    }, {
        key: "removeClass",
        value: function(n) {
            var t = !0
              , r = !1
              , a = void 0;
            try {
                for (var l, i = this.ele[Symbol.iterator](); !(t = (l = i.next()).done); t = !0) {
                    var s = l.value
                      , o = s.className.split(" ");
                    s.className = e.filter(o, function(e) {
                        return e !== n
                    }).join(" ")
                }
            } catch (u) {
                r = !0,
                a = u
            } finally {
                try {
                    !t && i["return"] && i["return"]()
                } finally {
                    if (r)
                        throw a
                }
            }
            return this
        }
    }, {
        key: "getOrigionalElement",
        value: function(e) {
            return e ? this.ele[e] : this.ele
        }
    }]),
    e
}();
window.$ = function(e) {
    return Rail.getInstance(e)
}
,
window.addEventListener("load", function() {
    $("h1").addClass("ready"),
    $(".bio").addClass("ready"),
    $("li").addClass("ready")
});
